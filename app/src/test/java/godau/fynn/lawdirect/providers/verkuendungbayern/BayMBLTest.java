package godau.fynn.lawdirect.providers.verkuendungbayern;

import godau.fynn.lawdirect.model.ParagraphList;
import godau.fynn.lawdirect.network.Network;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BayMBLTest {

    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
    }

    @Test
    public void getPublicationItems() throws IOException {
        VerkuendungBayernPublication bayMBL = new BayMBL();
        List<ParagraphList> list = bayMBL.getPublicationItems(0);

        assertEquals(15, list.size());

        assertTrue(list.get(0).getId().startsWith("202"));

    }
}