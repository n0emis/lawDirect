package godau.fynn.lawdirect.providers;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.*;
import godau.fynn.lawdirect.network.Network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestProvider extends LawProvider {

    @Override
    public List<ParentItem> getLawItems() {
        ArrayList<ParentItem> parentItems = new ArrayList<>();

        parentItems.add(new Book() {
            @Override
            public String getShortcode() {
                return "FYNN1";
            }

            @Override
            public List<Paragraph> getParagraphs(int page) {

                if (page > 0) return null;

                ArrayList<Paragraph> paragraphs = new ArrayList<>();
                paragraphs.add(new Paragraph("1", null, new SpannedString("Die Würde des Main-Spessart-Expresses ist unbegreifbar.")));
                paragraphs.add(new Paragraph("2", null, new SpannedString("Sie zu achten und zu schützen ist Verpflichtung der DB Regio AG Bayern.")));

                return paragraphs;
            }

            @Override
            public String getTitle() {
                return "Gesetzbuch";
            }

            @Override
            public Spanned getText() {
                return null;
            }
        });

        parentItems.add(new Publication() {
            @Override
            public List<ParagraphList> getPublicationItems(int page) {

                if (page > 0) return null;

                List<ParagraphList> paragraphListList = new ArrayList<>();

                paragraphListList.add(new ParagraphList() {
                    @Override
                    public List<Paragraph> getParagraphs(int page) throws IOException {

                        List<Paragraph> paragraphList = new ArrayList<>();

                        switch (page) {
                            case 0:
                                paragraphList.add(new Paragraph("0", "Vorläufige Überschrift", new SpannedString("Die Überschriften in dieser Verordnung sind unverzüglich zu ändern.")));
                                return paragraphList;
                            case 1:
                                paragraphList.add(new Paragraph("1", "Inkraftreten", new SpannedString("Diese Verkündung tritt mit dem auf den Verkündungstag foldengen Tag in Kraft.")));
                                return paragraphList;
                            case 2:
                                String text = Network.string(getNetwork().request("https://notabug.org/fynngodau/DSBDirect/raw/master/versionCode", null, "GET"), "UTF-8");
                                paragraphList.add(new Paragraph(text, "Version", new SpannedString("Version code")));
                                return paragraphList;
                            default:
                                return null;

                        }
                    }

                    @Override
                    public String getTitle() {
                        return "Regelungen zu Überschriften";
                    }

                    @Nullable
                    @Override
                    public Spanned getText() {
                        return new SpannedString("RzÜ");
                    }

                    @Override
                    public String getId() {
                        return "RzÜ";
                    }
                });

                return paragraphListList;
            }

            @Override
            public String getTitle() {
                return "Verkündungen";
            }

            @Override
            public String getShortcode() {
                return "Vk";
            }

            @Nullable
            @Override
            public Spanned getText() {
                return null;
            }
        });

        return parentItems;
    }

    @Override
    public String getTitle() {
        return "TestProvider";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return "test";
    }
}
