package godau.fynn.lawdirect.providers.rechtnrw;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;

import java.util.ArrayList;
import java.util.List;

public class RechtNrwProvider extends LawProvider {
    @Override
    public List<ParentItem> getLawItems() {
        List<ParentItem> lawItems = new ArrayList<>();
        lawItems.add(new GVNrw());
        lawItems.add(new MBlNrw());
        return lawItems;
    }

    @Override
    public String getTitle() {
        return "recht.nrw.de";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return new SpannedString("Landesrecht NRW");
    }

    @Override
    public String getId() {
        return "recht.nrw.de";
    }
}
