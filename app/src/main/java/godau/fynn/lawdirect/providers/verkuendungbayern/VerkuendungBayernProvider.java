package godau.fynn.lawdirect.providers.verkuendungbayern;

import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;

import java.util.ArrayList;
import java.util.List;

public class VerkuendungBayernProvider extends LawProvider {
    @Override
    public List<ParentItem> getLawItems() {
        List<ParentItem> lawItems = new ArrayList<>();
        lawItems.add(new BayMBL());
        lawItems.add(new GVBl());
        return lawItems;
    }

    @Override
    public String getTitle() {
        return "BAYERN.RECHT";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return new SpannedString("Verkündungsplattform Bayern");
    }

    @Override
    public String getId() {
        return "verkuendung-bayern.de";
    }
}
