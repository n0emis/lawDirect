package godau.fynn.lawdirect.providers.database;

import android.text.Spanned;
import androidx.annotation.Nullable;
import androidx.room.Room;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.persistence.DownloadedParentItem;
import godau.fynn.lawdirect.persistence.ItemDao;
import godau.fynn.lawdirect.persistence.ItemDatabase;

import java.util.ArrayList;
import java.util.List;

public class DatabaseParentItem extends BaseItem implements ParentItem {

    private transient final DownloadedParentItem downloadedParentItem;

    private final String title, path;

    private static final int PAGE_LIMIT = 100; // too high limits cause an SQLite exception

    public DatabaseParentItem(DownloadedParentItem downloadedParentItem) {
        this.downloadedParentItem = downloadedParentItem;
        title = downloadedParentItem.getListItem().getTitle();
        path = downloadedParentItem.getListItem().getId();
    }

    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) {

        ItemDao items = Room.databaseBuilder(getContext(), ItemDatabase.class, ItemDatabase.DATABASE_NAME).build()
                .getItemDao();

        int offset = page * PAGE_LIMIT;

        if (downloadedParentItem.getContent().size() <= offset) {
            return null;
        }

        List<String> ids = new ArrayList<>();

        for (int i = offset; i < downloadedParentItem.getContent().size() && i < offset + PAGE_LIMIT; i++) {
            ids.add(downloadedParentItem.getContent().get(i).getId());
        }

        List<DownloadedParentItem> downloadedParentItems = items.getParentItemsById(ids);

        List<ListItem> result = new ArrayList<>();

        for (int i = 0; i < downloadedParentItems.size(); i++) {
            if (downloadedParentItems.get(i).getContent().size() > 0) {
                result.add(new DatabaseParentItem(downloadedParentItems.get(i)));
            } else {
                result.add(downloadedParentItem.getContent().get(offset + i));
            }
        }

        return result;

    }

    @Override
    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public Spanned getText() {
        if (downloadedParentItem != null) {
            return downloadedParentItem.getListItem().getText();
        } else {
            return null;
        }
    }

    @Override
    public String getId() {
        return path;
    }
}
