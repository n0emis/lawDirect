package godau.fynn.lawdirect.providers.verkuendungbayern;

import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.NumberedItem;
import godau.fynn.lawdirect.model.Paragraph;
import godau.fynn.lawdirect.model.ParagraphList;
import godau.fynn.lawdirect.model.UnnumberedItem;
import godau.fynn.lawdirect.network.Network;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VerkuendungBayernParagraphList extends ParagraphList {

    private String fundstelle, titel, url;

    public VerkuendungBayernParagraphList(String fundstelle, String titel, String url) {
        this.fundstelle = fundstelle;
        this.titel = titel;
        this.url = url;
    }

    @Override
    public List<Paragraph> getParagraphs(int page) throws IOException {

        if (page > 0) return null;

        List<Paragraph> list = new ArrayList<>();

        Document d = Jsoup.parse(
                Network.string(getNetwork().request(url, null, "GET"))
        );


        Spanned spanned = Html.fromHtml(d.getElementById("documentbox").html());

        list.add(new UnnumberedItem(spanned));

        return list;
    }

    @Override
    public String getTitle() {
        return fundstelle;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return new SpannedString(titel);
    }

    @Override
    public String getId() {
        String[] urlParts = url.split("/");
        return urlParts[urlParts.length - 1];
    }
}
