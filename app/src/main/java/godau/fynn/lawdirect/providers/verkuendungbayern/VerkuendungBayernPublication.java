package godau.fynn.lawdirect.providers.verkuendungbayern;

import godau.fynn.lawdirect.model.ParagraphList;
import godau.fynn.lawdirect.model.Publication;
import godau.fynn.lawdirect.network.Network;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class VerkuendungBayernPublication extends Publication {
    @Override
    public List<ParagraphList> getPublicationItems(int page) throws IOException {

        List<ParagraphList> publicationItems = new ArrayList<>();

        Document d = Jsoup.parse(
                getNetwork().request(
                        getUrl() + "?offset=" + ((page * 15) + 1),
                        null, "GET"
                ), "UTF-8", getUrl()
        );

        if (d.getElementsByClass("result-count").text().contains("Keine Treffer gefunden"))
            return null;

        Elements trs = d.getElementsByTag("tr");
        for (int i = 1; i < trs.size(); i++) {
            Element tr = trs.get(i);

            Element fundstelleTd = tr.getElementsByAttributeValue("data-label", "Fundstelle").first();

            String url =
                    fundstelleTd.getElementsByTag("a").first().attr("abs:href");

            String fundstelle = fundstelleTd.text();
            String titel = tr.getElementsByAttributeValue("data-label", "Titel").text();

            publicationItems.add(
                    new VerkuendungBayernParagraphList(fundstelle, titel, url)
            );

        }

        return publicationItems;
    }

    abstract String getUrl();
}
