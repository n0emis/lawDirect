package godau.fynn.lawdirect.providers.database;

import android.content.Context;
import android.text.Spanned;
import androidx.annotation.Nullable;
import androidx.room.Room;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.persistence.DownloadedParentItem;
import godau.fynn.lawdirect.persistence.ItemDao;
import godau.fynn.lawdirect.persistence.ItemDatabase;

import java.util.ArrayList;
import java.util.List;

public class DatabaseProvider extends LawProvider {

    @Override
    public List<ParentItem> getLawItems() {

        ItemDao items = Room.databaseBuilder(getContext(), ItemDatabase.class, ItemDatabase.DATABASE_NAME).build()
                .getItemDao();

        List<ParentItem> parentItems = new ArrayList<>();

        for (DownloadedParentItem downloadedParentItem : items.getRootParentItems()) {
            parentItems.add(new DatabaseParentItem(downloadedParentItem));
        }
        return parentItems;
    }

    @Override
    public String getTitle() {
        return "Offline storage";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return "offline";
    }
}
