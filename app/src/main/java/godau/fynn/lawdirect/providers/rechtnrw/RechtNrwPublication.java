package godau.fynn.lawdirect.providers.rechtnrw;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import godau.fynn.lawdirect.model.ParagraphList;
import godau.fynn.lawdirect.model.Publication;
import godau.fynn.lawdirect.network.Network;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class RechtNrwPublication extends Publication {

    private transient NodeList nList;

    @Override
    public List<ParagraphList> getPublicationItems(int page) throws IOException {
        List<ParagraphList> publicationItems = new ArrayList<>();

        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();

        try {
            if (nList == null) {
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputStream rssStream = getNetwork().request(this.getUrl(), null, "GET");
                Document rssDoc = builder.parse(rssStream);
                rssDoc.getDocumentElement().normalize();
                nList = rssDoc.getElementsByTagName("item");
            }

            if (nList.item(page) != null) {
                Element rssItem = (Element) nList.item(page);
                String link = rssItem.getElementsByTagName("link").item(0).getTextContent();

                org.jsoup.nodes.Document d = Jsoup.parse(
                        getNetwork().request(link, null, "GET"), "ISO-8859-1", link
                );
                org.jsoup.select.Elements tables = d.select("table[cellspacing]");
                for (org.jsoup.nodes.Element table : tables) {
                    org.jsoup.select.Elements trs = table.getElementsByTag("tr");
                    trs.remove(0);
                    Collections.reverse(trs);
                    for (org.jsoup.nodes.Element tr : trs) {
                        org.jsoup.select.Elements tds = tr.getElementsByTag("td");
                        String seite = tds.get(4).text();
                        String datum = tds.get(1).text();
                        String description = tds.get(3).getElementsByTag("a").first().text();
                        String url = tds.get(3).getElementsByTag("a").first().attr("abs:href");
                        publicationItems.add(
                                new RechtNrwParagraphList("Seite " + seite + " - " + datum, description, url)
                        );
                    }
                }
            } else {
                return null;
            }

        } catch (ParserConfigurationException | SAXException e) {
            throw new IOException(e);
        }
        return publicationItems;
    }

    abstract String getUrl();
}
