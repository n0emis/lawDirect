package godau.fynn.lawdirect.providers.verkuendungbayern;

public class GVBl extends VerkuendungBayernPublication {
    @Override
    public String getTitle() {
        return "Gesetz- und Verordnungsblatt";
    }

    @Override
    public String getShortcode() {
        return "GVBl";
    }

    @Override
    String getUrl() {
        return "https://www.verkuendung-bayern.de/gvbl/";
    }
}
