package godau.fynn.lawdirect.providers;

import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.database.DatabaseProvider;
import godau.fynn.lawdirect.providers.rechtnrw.RechtNrwProvider;
import godau.fynn.lawdirect.providers.verkuendungbayern.VerkuendungBayernProvider;

import java.util.ArrayList;
import java.util.List;

public class LawProviderProvider extends BaseItem implements ParentItem {
    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) {
        if (page == 0) {
            List<LawProvider> lawProviders = new ArrayList<>();

            lawProviders.add(new VerkuendungBayernProvider());
            lawProviders.add(new RechtNrwProvider());
            lawProviders.add(new TestProvider());
            lawProviders.add(new DatabaseProvider());

            return lawProviders;
        } else return null;
    }

    @Override
    public String getTitle() {
        return "Law providers";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return "";
    }
}
