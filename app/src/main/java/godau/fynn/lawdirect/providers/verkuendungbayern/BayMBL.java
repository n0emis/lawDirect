package godau.fynn.lawdirect.providers.verkuendungbayern;

public class BayMBL extends VerkuendungBayernPublication {

    @Override
    public String getTitle() {
        return "Bayerisches Ministerialblatt";
    }

    @Override
    public String getShortcode() {
        return "BayMBL";
    }

    @Override
    String getUrl() {
        return "https://www.verkuendung-bayern.de/baymbl/";
    }
}
