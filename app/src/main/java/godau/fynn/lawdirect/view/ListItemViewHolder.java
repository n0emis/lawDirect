package godau.fynn.lawdirect.view;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.activity.FragmentFactory;
import godau.fynn.lawdirect.activity.GenericContentFragment;
import godau.fynn.lawdirect.activity.MainActivity;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.persistence.DownloadRunnable;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    private final FragmentActivity context;

    private final TextView title;
    private final TextView content;
    private final CardView card;

    private final String path;


    public ListItemViewHolder(@NonNull View itemView, String path, FragmentActivity context) {
        super(itemView);

        this.context = context;

        title = itemView.findViewById(R.id.title);
        content = itemView.findViewById(R.id.content);
        card = itemView.findViewById(R.id.card);

        this.path = path;

    }

    public void bind(final ListItem listItem) {

        if (listItem.getTitle() == null)
            title.setVisibility(View.GONE);
        else {
            title.setVisibility(View.VISIBLE);
            title.setText(listItem.getTitle());
        }

        if (listItem.getText() == null)
            content.setVisibility(View.GONE);
        else {
            content.setVisibility(View.VISIBLE);
            content.setText(listItem.getText());
        }

        if (listItem instanceof ParentItem) {
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment, FragmentFactory.getGenericFragment((ParentItem) listItem, path))
                            .addToBackStack(null)
                            .commit();
                }
            });
        }

        if (listItem instanceof LawProvider) {
            card.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    new Thread(new DownloadRunnable((LawProvider) listItem, context)).start();
                    return true;
                }
            });
        }
    }
}
