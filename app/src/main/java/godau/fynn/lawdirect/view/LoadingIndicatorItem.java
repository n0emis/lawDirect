package godau.fynn.lawdirect.view;

import android.content.Context;
import android.text.Spanned;
import androidx.annotation.Nullable;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;

public class LoadingIndicatorItem implements ListItem {
    @Override
    public String getTitle() {
        return "loading";
    }

    @Nullable
    @Override
    public Spanned getText() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setContext(Context context) {
    }
}
