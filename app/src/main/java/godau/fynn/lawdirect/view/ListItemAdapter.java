package godau.fynn.lawdirect.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;

import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ID_DEFAULT = 0;
    private static final int ID_LOADING_INDICATOR = 1;
    private final List<? extends ListItem> listItems;
    private final FragmentActivity context;
    private final String path;

    private final LayoutInflater inflater;

    public ListItemAdapter(FragmentActivity context, String path, List<? extends ListItem> listItems) {
        this.listItems = listItems;
        this.context = context;
        this.path = path;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
            case ID_DEFAULT:
                View view = inflater.inflate(R.layout.row_list_item_card, parent, false);
                return new ListItemViewHolder(view, path, context);
            case ID_LOADING_INDICATOR:
                view = inflater.inflate(R.layout.row_loading_indicator, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListItemViewHolder) {
            ((ListItemViewHolder) holder).bind(listItems.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



    @Override
    public int getItemViewType(int position) {
        if (listItems.get(position) instanceof LoadingIndicatorItem) {
            return ID_LOADING_INDICATOR;
        } else {
            return ID_DEFAULT;
        }
    }
}
