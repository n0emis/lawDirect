package godau.fynn.lawdirect.persistence;

import android.content.Context;
import android.util.Log;
import androidx.room.Room;
import godau.fynn.lawdirect.model.LawProvider;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.LawProviderProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DownloadRunnable implements Runnable {

    private final ParentItem root;
    private final Context context;

    private ItemDao items;

    private final List<DownloadedListItem> insertList = new ArrayList<>();

    public DownloadRunnable(LawProvider root, Context context) {
        this.root = root;
        this.context = context;
    }

    @Override
    public void run() {
        ItemDatabase database = Room.databaseBuilder(context, ItemDatabase.class, "database").build();
        items = database.getItemDao();

        DownloadedListItem listItem = new DownloadedListItem(root, new LawProviderProvider());
        insertList.add(listItem);
        try {
            downloadRecursively(root, listItem);

            // Insert all items
            items.insert(insertList);

            Log.d("DOWNLOAD", "Successfully downloaded " + insertList.size() + " items");

        } catch (IOException e) {
            // Download unsuccessful; discard paritally downloaded data
            e.printStackTrace();
        }
    }

    private void downloadRecursively(ParentItem parent, DownloadedListItem parentItem) throws IOException {
        List<? extends ListItem> list;
        int page = 0;
        do {
            list = parent.getContent(page++);

            if (list != null) {

                for (ListItem item : list) {
                    DownloadedListItem downloadedListItem = new DownloadedListItem(item, parentItem);

                    Log.d("DOWNLOAD", "Downloaded " + downloadedListItem.getId());
                    insertList.add(downloadedListItem);

                    if (item instanceof ParentItem) {
                        item.setContext(context);
                        downloadRecursively((ParentItem) item, downloadedListItem);
                    }
                }
            }

        } while (list != null);
    }
}
