package godau.fynn.lawdirect.persistence;

import android.text.Spanned;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import godau.fynn.lawdirect.model.BaseItem;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;

@Entity(tableName = "listItem")
public class DownloadedListItem extends BaseItem implements ListItem {

    @PrimaryKey
    @NonNull
    private final String id;

    private String parentId;

    private final String title;

    @TypeConverters(Converter.class)
    private final Spanned text;

    public DownloadedListItem(String title, Spanned text, String id, DownloadedListItem parent) {
        this(title, text, id, parent.getId());
    }


    DownloadedListItem(String title, Spanned text, String id, String parentId) {
        this(title, text, id);
        this.parentId = parentId;
    }

    @Ignore
    public DownloadedListItem(String title, Spanned text, String id) {
        this.title = title;
        this.text = text;
        this.id = id;
    }

    /**
     * Copies title, text and id from ListItem
     */
    @Ignore
    public DownloadedListItem(ListItem download, @Nullable DownloadedListItem parent) {
        this(download, parent.getId());
    }

    /**
     * Queries title, text and id from ListItem. Use if parent item
     * has not been downloaded.
     */
    @Ignore
    public DownloadedListItem(ListItem download, ParentItem parent) {
        this(download, parent.getId());
    }

    @Ignore
    private DownloadedListItem(ListItem download, String parentId) {
        this.title = download.getTitle();
        this.text = download.getText();
        this.id = parentId + "/" + download.getId();
        this.parentId = parentId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public Spanned getText() {
        return text;
    }

    public String getParentId() {
        return parentId;
    }

    public String getId() {
        return id;
    }
}
