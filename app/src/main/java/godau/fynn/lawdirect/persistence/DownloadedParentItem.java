package godau.fynn.lawdirect.persistence;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.io.Serializable;
import java.util.List;

public class DownloadedParentItem implements Serializable {
    @Embedded
    private final DownloadedListItem listItem;

    @Relation(parentColumn = "id", entityColumn = "parentId")
    private final List<DownloadedListItem> content;

    public DownloadedParentItem(DownloadedListItem listItem, List<DownloadedListItem> content) {
        this.listItem = listItem;
        this.content = content;
    }

    public DownloadedListItem getListItem() {
        return listItem;
    }

    public List<DownloadedListItem> getContent() {
        return content;
    }
}
