package godau.fynn.lawdirect.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public abstract class ItemDao {

    @Transaction
    @Query("SELECT * FROM listItem")
    public abstract List<DownloadedParentItem> getParentItems();

    @Transaction
    @Query("SELECT * FROM listItem WHERE parentId == ''")
    public abstract List<DownloadedParentItem> getRootParentItems();

    @Query("SELECT * FROM listItem")
    public abstract List<DownloadedListItem> getListItems();

    /**
     * @throws android.database.sqlite.SQLiteException if you pass too many parameters
     * @return <code>DownloadedParentItem</code>s for the items corresponding to the ids passed
     */
    @Transaction
    @Query("SELECT * FROM listItem WHERE id IN (:id) ORDER BY id DESC")
    public abstract List<DownloadedParentItem> getParentItemsById(List<String> id);

    @Insert
    public abstract void insert(List<DownloadedListItem> downloadedListItems);

    @Insert
    public abstract long insert(DownloadedListItem downloadedListItem);
}
