package godau.fynn.lawdirect.persistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = DownloadedListItem.class, version = 1)
public abstract class ItemDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "database";

    public abstract ItemDao getItemDao();

}
