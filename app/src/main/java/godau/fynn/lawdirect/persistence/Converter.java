package godau.fynn.lawdirect.persistence;

import android.text.*;
import androidx.room.TypeConverter;

@androidx.room.TypeConverters({String.class, Spanned.class})
public class Converter {

    @TypeConverter
    public static String toString(Spanned spanned) {
        if (spanned == null) return null;
        return Html.toHtml(spanned);
    }

    @TypeConverter
    public static Spanned toSpanned(String string) {
        if (string == null) return null;
        return new SpannedString(trimEndingNewlines(Html.fromHtml(string)));
    }

    private static CharSequence trimEndingNewlines(Spanned spanned) {
        int newEnd = spanned.length();
        for (int i = spanned.length() - 1; i >= 0; i--) {
            char c = spanned.charAt(i);
            if (c == '\u0000')
                continue;
            else if (c == '\n') {
                newEnd = i;
            } else {
                return spanned.subSequence(0, newEnd);
            }
        }
        return spanned;
    }
}
