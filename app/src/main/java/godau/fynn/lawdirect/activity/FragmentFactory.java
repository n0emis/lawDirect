package godau.fynn.lawdirect.activity;

import android.os.Bundle;
import godau.fynn.lawdirect.model.ParentItem;

public abstract class FragmentFactory {

    public static GenericContentFragment getGenericFragment(ParentItem parentItem, String path) {
        GenericContentFragment fragment = new GenericContentFragment();
        Bundle args = new Bundle();
        args.putSerializable("law_item", parentItem);
        args.putString("path", path);
        fragment.setArguments(args);
        return fragment;
    }
}
