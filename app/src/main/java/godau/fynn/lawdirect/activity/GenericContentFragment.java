package godau.fynn.lawdirect.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.lawdirect.R;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.providers.LawProviderProvider;
import godau.fynn.lawdirect.view.ListItemAdapter;
import godau.fynn.lawdirect.view.LoadingIndicatorItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenericContentFragment extends Fragment {

    private final List<ListItem> content = new ArrayList<>();

    private ProgressBar progressBar;
    private TextView text;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    private ParentItem parentItem;
    private String path;

    private int page;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey("law_item")) {
            parentItem = (ParentItem) getArguments().getSerializable("law_item");
        } else {
            parentItem = new LawProviderProvider();
        }

        parentItem.setContext(getContext());

        if (getArguments() != null && getArguments().containsKey("path")) {
            path = getArguments().getString("path");
            path += "/" + parentItem.getId();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.generic_content_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressBar = view.findViewById(R.id.progress);
        text = view.findViewById(R.id.text);

        recyclerView = view.findViewById(R.id.recycler_view);

        getActivity().getActionBar().setTitle(parentItem.getTitle());

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new ListItemAdapter(getActivity(), path, content));

        if (content.size() == 0) {
            loadContent(parentItem, 0);
        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.addOnScrollListener(new LazyLoadScrollListener());
        }
    }

    private void loadContent(final ParentItem parentItem, final int page) {

        Log.d(GenericContentFragment.class.getSimpleName(), "loading page " + page);

        if (page > 0) {
            content.add(new LoadingIndicatorItem());
            recyclerView.getAdapter().notifyItemInserted(content.size() - 1);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final List<? extends ListItem> listItems = parentItem.getContent(page);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (page > 0) {
                                content.remove(content.size() - 1);
                                recyclerView.getAdapter().notifyItemRemoved(content.size());
                            }

                            if (listItems != null) {

                                content.addAll(listItems);


                                recyclerView.getAdapter().notifyItemRangeInserted(
                                        recyclerView.getAdapter().getItemCount() - listItems.size(),
                                        listItems.size()
                                );

                                RecyclerView.OnScrollListener scrollListener = new LazyLoadScrollListener();
                                recyclerView.addOnScrollListener(scrollListener);

                                scrollListener.onScrolled(recyclerView, 0, 0);
                            }
                        }
                    });

                } catch (IOException e) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text.setText(R.string.network_error);
                        }
                    });

                    e.printStackTrace();
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }

            }
        }).start();
    }

    private class LazyLoadScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (layoutManager.findLastVisibleItemPosition() >= content.size() - 15) {
                recyclerView.clearOnScrollListeners();
                loadContent(parentItem, ++page);
            }
        }
    }
}
