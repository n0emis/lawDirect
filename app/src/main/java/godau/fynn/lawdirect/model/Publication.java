package godau.fynn.lawdirect.model;

import android.text.Spanned;
import android.text.SpannedString;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Represents a publication consisting of legal documents
 */
public abstract class Publication extends BaseItem implements ParentItem, Serializable {

    /**
     * @see #getContent(int)
     */
    public abstract List<ParagraphList> getPublicationItems(int page) throws IOException;

    public abstract String getShortcode();

    public List<? extends ListItem> getContent(int page) throws IOException {
        return getPublicationItems(page);
    }

    public Spanned getText() {
        return new SpannedString(getShortcode());
    }

    public String getId() {
        return getShortcode();
    }
}
