package godau.fynn.lawdirect.model;

import android.content.Context;
import android.text.Spanned;
import androidx.annotation.Nullable;

/**
 * Content that is displayable
 */
public interface ListItem {
    public String getTitle();

    public @Nullable Spanned getText();

    public String getId();

    public void setContext(Context context);
}
