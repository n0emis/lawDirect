package godau.fynn.lawdirect.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public abstract class ParagraphList extends BaseItem implements ListItem, ParentItem, Serializable {

    /**
     * @see #getContent(int)
     */
    public abstract List<Paragraph> getParagraphs(int page) throws IOException;

    public abstract String getTitle();

    public List<? extends ListItem> getContent(int page) throws IOException {
        return getParagraphs(page);
    }
}
