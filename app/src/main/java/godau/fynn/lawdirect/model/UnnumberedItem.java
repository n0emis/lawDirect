package godau.fynn.lawdirect.model;

import android.text.Spanned;
import androidx.annotation.Nullable;

/**
 * TODO: The current database implementation only
 * supports one unnumbered item per paragraph list.
 */
public class UnnumberedItem extends Paragraph {

    private final Spanned text;

    public UnnumberedItem(Spanned text) {
        this.text = text;
    }

    @Nullable
    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public Spanned getText() {
        return text;
    }
}