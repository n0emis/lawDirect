package godau.fynn.lawdirect.model;

import java.io.IOException;
import java.util.List;

public abstract class LawProvider extends SinglePageParentItem {

    /**
     * @return A list typically consisting of {@link Book}s and {@link Publication}s
     */
    public abstract List<ParentItem> getLawItems() throws IOException;

    public List<? extends ListItem> getContent() throws IOException {
        return getLawItems();
    }
}
