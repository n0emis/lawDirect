package godau.fynn.lawdirect.model;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Content that contains more displayable content
 */
public interface ParentItem extends ListItem, Serializable {

    /**
     * @param page Index of the page to load; the first page is 0
     * @return <code>null</code> if the page doesn't exist, a list of its content otherwise
     */
    public @Nullable List<? extends ListItem> getContent(int page) throws IOException;
}
