package godau.fynn.lawdirect.model;

import android.text.Spanned;
import androidx.annotation.Nullable;

import java.io.Serializable;

public class Paragraph extends BaseItem implements ListItem, Serializable {
    private String number = "";
    private @Nullable String title;
    private Spanned text;

    public Paragraph(String number, @Nullable String title, Spanned text) {
        this.number = number;
        this.title = title;
        this.text = text;
    }

    protected Paragraph() {}

    public String getNumber() {
        return number;
    }

    public String getTitle() {
        if (title == null)
            return "§" + number;
        else
            return "§" + number + ' ' + title;
    }

    public Spanned getText() {
        return text;
    }

    public String getId() {
        return getNumber();
    }
}
