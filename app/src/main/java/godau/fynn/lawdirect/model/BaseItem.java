package godau.fynn.lawdirect.model;

import android.content.Context;
import androidx.annotation.NonNull;
import godau.fynn.lawdirect.network.Network;

import java.io.Serializable;

/**
 * Provides context to subclasses
 */
public abstract class BaseItem implements ListItem, Serializable {

    private transient Context context;
    private transient Network network;

    public void setContext(@NonNull Context context) {
        this.context = context;
    }

    protected @NonNull
    Context getContext() {
        return context;
    }

    protected @NonNull
    Network getNetwork() {
        if (network == null) {
            network = new Network(context);
        }
        return network;
    }
}
