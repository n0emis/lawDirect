package godau.fynn.lawdirect.model;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.List;

public abstract class SinglePageParentItem extends BaseItem implements ParentItem {

    @Nullable
    @Override
    public List<? extends ListItem> getContent(int page) throws IOException {
        if (page == 0) {
            return getContent();
        } else {
            return null;
        }
    }

    public abstract @Nullable
    List<? extends ListItem> getContent() throws IOException;
}
