package godau.fynn.lawdirect.model;

import java.io.IOException;
import java.util.List;

/**
 * Object representing a law statue book
 */
public abstract class Book extends ParagraphList implements ParentItem {
    public abstract String getShortcode();

    public String getId() {
        return getShortcode();
    }
}
