package godau.fynn.lawdirect.model;

import android.text.Spannable;

public class NumberedItem extends Paragraph {

    public NumberedItem(String number, String title, Spannable text) {
        super(number, title, text);
    }

}
