package godau.fynn.lawdirect.persistence;

import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import godau.fynn.lawdirect.model.ListItem;
import godau.fynn.lawdirect.model.ParentItem;
import godau.fynn.lawdirect.network.Network;
import godau.fynn.lawdirect.providers.LawProviderProvider;
import godau.fynn.lawdirect.providers.TestProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ItemDatabaseTest {

    private static ItemDao items;
    @BeforeClass
    public static void setUp() {
        Network.assumeNetworkAvailable = true;
        ItemDatabase database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), ItemDatabase.class).build();
        items = database.getItemDao();
    }

    @Test
    public void downloadTestProvider() throws IOException {

        ParentItem provider = new TestProvider();
        DownloadedListItem listItem = new DownloadedListItem(provider, new LawProviderProvider());
        items.insert(listItem);
        System.out.println("Inserting " + listItem.getId());
        downloadRecursively(provider, listItem);

        for (DownloadedListItem item :
                items.getListItems()) {
            System.out.println(item.getTitle() + " with id = " + item.getId() + " and parentId = " + item.getParentId());
        }

        for (DownloadedParentItem item :
                items.getParentItems()) {
            System.out.println(item.getListItem().getTitle() + " with amount of items = " + item.getContent().size());
        }

        assertEquals(1, items.getRootParentItems().size());

        assertEquals("TestProvider", items.getRootParentItems().get(0).getListItem().getTitle());

        assertEquals(2, items.getRootParentItems().get(0).getContent().size());

        assertEquals("Gesetzbuch", items.getRootParentItems().get(0).getContent().get(0).getTitle());

        assertEquals("/test/FYNN1", items.getRootParentItems().get(0).getContent().get(0).getId());

    }

    private void downloadRecursively(ParentItem parent, DownloadedListItem parentItem) throws IOException {
        List<? extends ListItem> list;
        int page = 0;
        do {
            list = parent.getContent(page++);

            if (list != null) {

                for (ListItem item : list) {
                    DownloadedListItem downloadedListItem = new DownloadedListItem(item, parentItem);

                    System.out.println("Inserting " + downloadedListItem.getId());
                    items.insert(downloadedListItem);

                    if (item instanceof ParentItem) {
                        downloadRecursively((ParentItem) item, downloadedListItem);
                    }
                }
            }

        } while (list != null);
    }

}